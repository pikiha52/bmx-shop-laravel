<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Part;
use Illuminate\Support\Facades\DB;

class PartsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('bmx.parts');
    }

    public function bars()
    {
        
        $parts = Part::where('category', 1)->get();
        return view('parts/bars', ['h3' => 'BARS'], ['parts' => $parts]);
    }

    public function breaking()
    {
        $parts = Part::where('category', 2)->get();
        return view('parts/bars', ['h3' => 'BREAKING'], ['parts' => $parts]);
    }

    public function chains()
    {
        $parts = Part::where('category', 3)->get();
        return view('parts/bars', ['h3' => 'CHAINS'], ['parts' => $parts]);
    }

    public function cranks()
    {
        $parts = Part::where('category', 4)->get();
        return view('parts/bars', ['h3' => 'CRANKS & BB'], ['parts' => $parts]);
    }

    public function forks()
    {
        $parts = Part::where('category', 5)->get();
        return view('parts/bars', ['h3' => 'FORKS'], ['parts' => $parts]);
    }

    public function rims()
    {
        $parts = Part::where('category', 6)->get();
        return view('parts/bars', ['h3' => 'RIMS'], ['parts' => $parts]);
    }

    public function frame()
    {
        $parts = Part::where('category', 19)->get();
        return view('parts/bars', ['h3' => 'FRAMES'], ['parts' => $parts]);
    }
    
    public function completebikes()
    {
        $parts = Part::where('category', 20)->get();
        return view('parts/bars', ['h3' => 'COMPLETE BIKES'], ['parts' => $parts]);
    }

    public function safetygear()
    {
        $parts = Part::where('category', 21)->get();
        return view('parts/bars', ['h3' => 'SAFETY GEAR'], ['parts' => $parts]);
    }

    public function grips()
    {
        $parts = Part::where('category', 7)->get();
        return view('parts/bars', ['h3' => 'GRIPS'], ['parts' => $parts]);
    }

    public function headset()
    {
        $parts = Part::where('category', 8)->get();
        return view('parts/bars', ['h3' => 'HEADSET'], ['parts' => $parts]);
    }

    public function hubs()
    {
        $parts = Part::where('category', 9)->get();
        return view('parts/bars', ['h3' => 'HUBS'], ['parts' => $parts]);
    }

    public function pedals()
    {
        $parts = Part::where('category', 10)->get();
        return view('parts/bars', ['h3' => 'HUBS'], ['parts' => $parts]);
    }

    public function pegs()
    {
        $parts = Part::where('category', 11)->get();
        return view('parts/bars', ['h3' => 'HUBS'], ['parts' => $parts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $part = Part::find($id);
        $related = Part::orderBy('merk', 'desc')->take(8)->get();
        $related = Part::orderBy('merk', 'desc')->limit(8)->get();

        return view('parts.show', compact('related'), compact('part'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
