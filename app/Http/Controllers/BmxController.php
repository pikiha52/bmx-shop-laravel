<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class BmxController extends Controller
{
    public function index()
    {
        $blogs = Blog::find([1, 2, 3]);
        return view('bmx/index', ['blog' => $blogs]);
    }

    public function info()
    {
        return view('bmx.info');
    }
}
