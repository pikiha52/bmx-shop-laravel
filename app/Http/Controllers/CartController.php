<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Part;

class CartController extends Controller
{
    public function addToCart(Request $request)
    {
        
    $this->validate($request, [
        'id' => 'required|exists:parts,id', 
        'qty' => 'required|integer'
    ]);

    $carts = json_decode($request->cookie('dw-carts'), true); 
  
    if ($carts && array_key_exists($request->id, $carts)) {
       
        $carts[$request->id]['qty'] += $request->qty;
    } else {
       
        $part = Part::find($request->id);
       
        $carts[$request->id] = [
            'qty' => $request->qty,
            'id' => $part->id,
            'merk' => $part->merk,
            'price' => $part->price,
            'images' => $part->images
        ];
    }

 
    $cookie = cookie('dw-carts', json_encode($carts), 2880);
  
    return redirect()->back()->cookie($cookie);
    }

    public function listCart()
{
   
    $carts = json_decode(request()->cookie('dw-carts'), true);
    
    $subtotal = collect($carts)->sum(function($row) {
        return $row['qty'] * $row['harga']; 
    });
 
    return view('parts.cart', compact('carts', 'subtotal'));
}



}
