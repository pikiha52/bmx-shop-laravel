-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 11 Des 2020 pada 03.26
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bmx_shop`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@multi-auth.test', NULL, '$2y$10$lOwq44WfCUChLUYE.kETLuHXNxRE/uCDkjluM.ucmzKfotZgNQ0.O', NULL, '2020-11-27 02:37:01', '2020-11-27 02:37:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `blogs`
--

INSERT INTO `blogs` (`id`, `judul`, `isi`, `isi2`, `tag`, `gambar`, `url`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'RICH FORNE WELCOME TO FEDERAL 2017', 'Setelah rentetan cedera selama tiga tahun terakhir, Rich menemukan kesendirian di antara teman dekatnya di Federal dan meskipun dia merasa tidak pantas mendapatkannya, Federal sangat bangga menyambutnya ke dalam keluarga. Video barunya adalah bukti bahwa gairah melebihi sensibilitas. Rich sebelum bergabung dengan Federal sempat bergabung dengan team Subrosa pada tahun 2014. beberapa tahun terakhir berkerjasama dengan Federal bikes dalam pembuatan video edit. dia juga di kenal sebagai filmer dan editor karya-karya Rich sangat berbeda dengan video-video BMX pada umumnya dia lebih mendekatkan sudut pandang dalam rideing BMX yang berbeda. Karya seperti “Above Below” yang mendapat penghargaan video of the years 2014 NORA CUP', '', 'richforne', 'welcome-to-federal.png', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/p__CouH1h5o\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', NULL, '2020-11-17 17:00:00', NULL),
(2, 'INVESTMENT CASTING', 'Investment casting adalah istilah yang akhir-akhir ini sering muncul dalam penjelasan spesifikasi di hampir semua produk/part BMX, tapi apakah sebenarnya investment casting? Dari penampilan luarnya mungkin kalian berpikir bahwa itu adalah proses produksi yang menggunakan sihir yang bisa mengubah komponen sepeda kalian seperti fork, frame atau crank menjadi satu bongkah benda tanpa adanya pengelasan. Benar kan?  Sebenarnya masih kurang tepat, mari kita bedah sama-sama\r\n\r\nPertama gw mau bilang kalau investment casting itu bukan hal baru, faktanya hal adalah proses tertua untuk membentuk benda berbahan metal dalam sejarah manusia, bahkan sebelum BMX mengadopsinya, Proses ini digunakan untuk hampir semua jenis dari desain sepeda,  jadi bukan hal baru juga untuk dunia sepeda. Pada dasarnya ini adalah proses menuangkan besi cair panas kedalam cetakan (mold atau casting) untuk membuat bagian (headtube triangle, dropout fork, dropout chain stay) yang mempunyai desain rumit, menyita waktu dan biaya apabila diterapkan pada pengerjaan CNC dan lainya. Kebanyakan orang mengira baigian yang di cetak itu adalah satu bagian tanpa adanya sambungan  “bukan” tapi dibuat dengan sleeve atau sambungan yang berupa pipa diameter kecil masuk ke dalam pipa yang diameternya lebih besar  sebelum proses pengelasan, seperti layaknya pipa air pvc dengan sambungan L atau T.\r\n\r\nAda beberapa kelebihan terhadap investment cast, yang pertama estetika (keindahan) ukiran logo, bentuk seperti halnya dropout dari fork dan boss spindle dan crank juga melakukan hal yang sama. Kedua yang berkaitan dengan estetika adalah struktur karena pengelasan bisa ditempatkan diatas bagian yang mendapat tekanan tinggi seperti dropout leg junction sampai bagian atas dari bagian yang dicetak fork leg, hal ini dilakukan agar pengelasan bisa lebih halus tanpa mengorbankan kekuatan, teknik pengerjaan seperti ini membuat sebuah produk kelihatan seperti satu bongkahan yang menyatu tanpa adanya pengelasan.\r\n\r\n', 'Animal Street Fork Offset\r\n\r\nKelebihan lainnya adalah tidak seperti pengelasan biasa, bagian yang dicetak berada pada posisi yang aman pada saat didinginkan setelah pengelasan sehingga tidak merubah bentuknya. Panas ekstrim dari pengelasan tradisional bisa menyebabkan perubahan bentuk atau bahkan bisa mengotori pipa, karenanya hal ini bisa mengurangi jumlah pengelasan yang tidak perlu dilakukan pada bagian yang hanya terkena tekanan sedikit. Untuk harga part dengan spesifikasi invesment casting  biasanya di bandrol lebih mahal dari pada part yang masih mengunakan pengelasan tradisional.', 'tech', 'casting.jpg', 'download-14.jpg', NULL, '2020-11-17 17:00:00', NULL),
(3, 'BMX FRAME GEOMETRY', 'Ini terjadi setiap tahun ketika kita melihat beberapa Frame BMX yang baru dirilis atau versi terbaru dari model sebelumnya, kita pikir ini adalah saat yang tepat untuk melihat lebih dekat jumlah angka-angka geometri seperti 74.5, 13.25″ dan 71 benar-benar berarti bagi kalian dan bagaimana itu bisa mengubah cara bermain/mengendarai BMX kalian dan akan merasakan perbedaanya. Selama bertahun-tahun frame BMX telah disempurnakan berulang kali. tidak hanya menawarkan produk unggulan, namun frame bmx benar-benar dirancang lebih spesifik menyesuaikan gaya bermain kalian. Kita melihat penyesuaian kecil untuk mengikuti tren atau hanya sedikit detail yang ditambahkan ke frame untuk membuatnya lebih menonjol. Jika kalian ingin membeli sebuah frame baru, atau hanya ingin mengetahui bagaimana geometri Frame BMX bekerja read more.\r\n\r\nGeometri Frame BMX\r\n\r\nPanjang Tabung Atas / Top Tube Length\r\n\r\nPanjang tabung atas adalah salah satu dari ukuran yang sering kita lihat dengan sangat beragam pilihan dari 20 “(kadang lebih kecil di kisaran 19” atau lebih kecil lagi 18 “dan 16”) hingga 21 “dan bahkan 22”. Ada juga opsi 24 “dan 26” untuk MTB, tapi mari fokus di sini pada kisaran 20 “. Panjang tabung atas adalah ukuran jarak antara pusat head tube ke bagian tengah seat tube. Ukuran umumnya merupakan pilihan bagi rider berdasarkan tinggi badan dan seberapa banyak ruangan yang mereka inginkan. Biasanya kita melihat orang yang bertinggi badan di bawah 150cm mengunakan ukuran 20 – 20,5 inci, kisaran tinggi badan 160cm – 170cm bisa mengunakan 20,5 – 20,75 “dan orang-orang pempunyai tinggi badan lebih dari 180cm mengunakan frame dengan ukuran 21” +. Umumnya ini memberi ruang untuk kaki kalian dan memberi jangkauan lengan ke bar. Biasanya cukup mudah untuk melihat apakah kalian ada di sepeda yang terlalu besar atau terlalu kecil. sekali lagi, ukuran ini bisa menjadi pilihan.\r\n\r\nFrame yang lebih panjang juga bisa membuat sepeda terasa lebih stabil karena jarak roda akan semakin jauh, dimana frame yang lebih pendek akan mendekatkan sumbu roda sehingga membuatnya lebih responsif. Hal ini juga bisa pengaruhi oleh sudut head tube dan panjang chain stay dalam frame.\r\n\r\nKemiringan Tabung Depan / Head Tube Angle\r\n\r\nSudut head tube adalah sering kita lihat paling sedikit berubah dalam Geometri Frame BMX, namun memiliki efek yang jauh lebih besar dalam mengendarai sepeda BMX. Frame BMX memiliki antara sudut 74 sampai 76 derajat head tube. Kebanyakan frame BMX modern memiliki 75 derajat. Sudut head tube telah berubah dari trend seperti Trails atau Ramp menjadi yang paling populer di Street yang memanfaatkan lebih banyak keseimbangan didepan dan jarak yang lebih pendek di antara roda.\r\n\r\nBeberapa dari kalian mungkin bertanya-tanya bagaimana perubahan 2 derajat bisa berubah begitu banyak. Nah, di bawah ini kalian bisa menemukan grafik yang di susun menunjukkan bagaimana sudut bisa mengubah sesuatu.\r\n\r\nSudut head tube yang lebih tegak (76 derajat) akan membawa roda depan lebih dekat ke arah Anda, membuatnya lebih mudah untuk nose manual dan trik-trik yang mengunakan roda depan, bila di sudut (74 derajat) sedikit lebih sulit untuk nose manual atau trick-trick yang mengunakan roda depan. Dengan roda yang lebih jauh ke depan kalian memerlukan extra tenaga untuk melakuakan tick roda depan dan sudut 74 derajat ini lebih baik untuk putaran yang lebih bertahap seperti pada BMX dirt atau trails. Sudut yang lebih curam 76 derajat membawa celah di antara roda lebih dekat dan lebih responsif, jadi bar spin akan lebih cepat. Itulah sebabnya kita melihat banyak nose manual dan G Turn (memutar) roda depan akhir-akhir ini dari street rider yang berjalan di sudut head tube yang lebih curam.\r\n\r\nKemiringan Tabung Jok / Seat Tube Angle\r\n\r\nSudut tabung seat adalah komponen kunci lainnya pada keseluruhan Frame. Frame BMX menggunakan sudut tabung 71 derajat, namun kami menemukan sejumlah frame menggunakan sudut 70 derajat dan 69 derajat juga. Sudut tabung jok dapat mengubah keseluruhan panjang frame tanpa mengubah panjang tabung atas. Misalnya, frame top tube 21 inci dengan sudut tabung 71 derajat akan membuatnya seperti frame dengan top tube 21 beda halnya dengan 69 derajat\r\n\r\nNamun, jika kalian mengambil frame top tube 21” dan memasangkannya dengan sudut tabung 69 derajat, panjang top tube akan tetap sama namun sudut menggerakkan braket bagian bawah lebih dekat ke head tube, yang membuatnya terasa lebih seperti. sebuah frame 20,75 – 20,8 “, kita memberikannya kisaran karena ini juga dapat dipengaruhi oleh ketinggian seat tube. Seperti yang dapat Anda lihat dari gambar di atas, kita mendasarkan hitungan dari ketinggian tabung stand over 9 “, yang cukup standar. Jadi dari top tube ke bottom braket kita bisa melihat perbedaan 0,30 “antara dua sudut. Itu berarti sudut 69 derajat akan menempatkan bottom braket 0,30 “lebih dekat ke head tube.\r\n\r\nTinggi Tabung Jok / Seat Tube Height\r\n\r\nTinggi tabung seat (atau ketinggian standover) adalah tabung vertikal yang berada di antara tabung bagian atas dan bottom braket. Seperti yang telah disebutkan di atas, tabung ini memiliki sudut 71 derajat secara tradisional, ada juga frame dengan 70 atau 69. Tinggi tabung seat dari top tube ke down tube. Sebuah ketinggian seat tube yang lebih rendah, untuk contoh Tall Order 187 Frame memiliki ketinggian tabung 7,4 “dengan sudut 71 derajat Frame ini sangat populer di kalangan rider bmx park. Berbeda dengan Federal DLX yang memiliki tinggi tabung 9” dengan sudut 71 derajat lebih di sukai oleh rider-rider street pada saat ini.\r\n\r\nTidak hanya tingginya mengubah tampilan Frame, seperti yang bisa Anda lihat dari Frame Tall Order 187 yang sangat rendah. beberapa rider mengatakan bahwa ketinggian stand over yang lebih rendah membuat tailwhips menjadi lebih mudah, di mana stand over tinggi yang diuntungkan adalah membuat barspins lebih mudah, Ini sebagian besar merupakan selera pribadi, namun sudut seat tube bisa mengubah karakter berkendara kalian.\r\n\r\n', 'Tinggi Bracket Bawah / Botom Bracket Height\r\n\r\nTinggi braket bawah berdasarkan pada 20″ diameter roda BMX dan tinggi BB berada di atas pusat drop out. dari pusat drop out ke bagian bawah roda 20 “adalah 10”. Jadi, menaikkan atau menurunkan ketinggian bottom braket sebagian besar frame berkisar dari kisaran 11,5 “sampai 11,75”. Ketinggiannya bisa mengubah keseluruhan tampilan Frame. Ketinggian bottom braket dapat memberi  kestabilan dan kelambat dalam berputar seperti 180 dan 360 trick, di mana braket bawah yang lebih tinggi dapat membuatnya lebih mudah bergerak dan mudah berputar. Hal lain yang perlu diperhatikan adalah bahwa ketinggian braket bagian bawah juga bisa diubah dengan ukuran ban. 2,40″ atau 2,50 dan akan terasa lebih tinggi apa bila diperlukan.\r\n\r\nPanjang Pipa Bawah Rantai / Chain stay length (CS)\r\n\r\nChain stay length / Panjang chain stay adalah salah satu yang terpenting pada geometri frame BMX karna sangat berpengaruh pada riding style dari masing-masing rider. Contohnya pada Varanyak frame dari FIEND memiliki panjang CS 12,6″ jarak sumbu roda semakin dekat membuat lebih agresif  frame pendek seperti ini lebih cocok ke rider street technical. di bandingkan dengan frame trails seperti Fit Heartbreaker yang mempunyai CS 14,5″. Jarak ini membuat terasa lebih stabil karena sumbu roda lebih jauh lebih cocok ke rider trails. CS yang lebih pendek akan memudahkan manual dan lompatan, di mana CS yang lebih panjang akan membuat lebih sulit untuk manual dan lebih mudah untuk menjaga sepeda agar tetap stabil pada saat terbang. Ini kurang atau lebihnya preferensi tentang bagaimana gaya berkendara kalian masing-masing. Untuk lebih aman kalian bisa memilih bagian tengah di kisaran panjang 13-13,75” karna akan memberi kalian keseimbangan yang baik antara keduanya yang dan bisa ideal untuk orang-orang yang suka riding Ramp, Street dan Trails.\r\n\r\nJadi kurang lebih seperti itu bagaimana geometri Frame BMX bekerja. Dengan begitu banyak frame yang bisa dipilih, ada kemungkinan kalian bisa menemukan semua fitur berbeda yang sesuai yang kalian sukai. Entah itu sudut head tube yang curam, top tube pendek dan CS menengah.', 'news', 'download-15.jpg', 'download-17.jpg', NULL, '2020-11-17 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`id`, `nama_kategori`) VALUES
(1, 'Bars'),
(2, 'Braking'),
(3, 'Chains'),
(4, 'Cranks & BB'),
(5, 'Forks'),
(6, 'Rims'),
(7, 'Grips'),
(8, 'Headsets'),
(9, 'Hubs'),
(10, 'Pedals'),
(11, 'Pegs'),
(12, 'Seating'),
(13, 'Small Parts'),
(14, 'Spokes'),
(15, 'Sprocket'),
(16, 'Stems'),
(17, 'Tires & Tubes'),
(18, 'Tools'),
(19, 'Frame'),
(20, 'Safety Gear'),
(21, 'Complete Bikes');

-- --------------------------------------------------------

--
-- Struktur dari tabel `complete_bikes`
--

CREATE TABLE `complete_bikes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_11_15_101537_complete_bikes', 1),
(5, '2020_11_18_021537_blogs', 1),
(6, '2020_11_18_024721_parts', 1),
(7, '2020_11_27_085026_create_admins_table', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `parts`
--

CREATE TABLE `parts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images2` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images3` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images4` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images5` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(255) NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` int(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `parts`
--

INSERT INTO `parts` (`id`, `category`, `brand`, `merk`, `images`, `images2`, `images3`, `images4`, `images5`, `price`, `details`, `code`, `weight`, `is_active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '1', 'Subrosa', 'Subrosa Ray Bar 9', 'IMG_4739-300x300.jpg', 'IMG_4739.jpg', 'IMG_4739-600x450.jpg', '', '', 1300000, 'Some things never change. The feeling of a 12° back 1° up bar is one of those things, and that’s just how Matt Ray likes them. Our Deathproof Heating process, Subrosa brazed on badge, and E-Z cut grooves improve on the classic feel, and will have any bike feeling and looking better.\r\n\r\nMatt Ray signature model\r\n– 100% post weld Deathproof heat treatment\r\n– 13 butted 4130 chromoly\r\n– 29” wide / 12° back sweep / 1° up sweep\r\n– Sizes:  9.0”\r\n– Weight: 28.2 oz.\r\n– Colors: Black', 'BAR1', '1 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(2, '1', 'Shadow', 'Shadow Vultus Bar 10', 'IMG_4738-300x300.jpg', 'IMG_4738-600x450.jpg', '', '', '', 1400000, 'Vultus Featherweight and Vultus SG. The Featherweight features our lightweight 13B tubing profile. Thick where you need it, thinner in less stress areas. We have also reduced the size of our proprietary tear shape cross bar to realize even more weight savings. For the ultimate street bar, go with the Vultus SG. SG stands for Street Gauge and is a reference to the straight tubing that we use in the construction. Both bars offer the same popular geometry and have our 100% post-weld heat treatment.', 'BAR2', '1 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(3, '1', 'Animal', 'Animal Empire Bar 10', 'IMG_4736-300x300.jpg', 'IMG_4736.jpg', '', '', '', 1300000, 'The Colin Varanyak signature bar. Multi Butted Chromoly 2 pc Bars. Available 9.5” and 9.0” rise. Post Weld Heat Treatment. Rise: 9″ Width: 29″ Backsweep: 12° Upsweep: 1° Crossbar Height: 7.00″ Crossbar Width: 11.50″ Rise: 9.0″ Width: 28.75″ Backsweep: 12° LIFETIME WARRANTY against breakage.', 'BAR3', '1 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(4, '1', 'Animal', 'Animal Foursome Bar 9', 'IMG_4742-300x300.jpg', 'IMG_4742.jpg', '', '', '', 1300000, 'Multi Butted Chromoly 4 pc Bars. Post Weld Heat Treatment. Rise: 9″ Width: 29″ Backsweep: 11° Upsweep: 5° Crossbar Height: 6″ Crossbar Width: 10″ Color – Black LIFETIME WARRANTY against breakage.', 'BAR4', '1 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(5, '1', 'Terrible', 'Terrible One Salt City Bar', 'IMG_4741-300x300.jpg', 'IMG_4741.jpg', '', '', '', 1500000, 'Shawn Elf Walters signature handlebars by T1. These bars are made from heat treated multi butted 4130 chromoly and feature a 4 piece design with the T-1 logo cut out of the end caps. These bars are 8.2 Rise and 28 wide.', 'BAR5', '1 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(6, '1', 'Subrosa', 'Subrosa Freecoaster Bar', 'IMG_4743-300x300.jpg', 'IMG_4743.jpg', '', '', '', 1400000, 'The Subrosa 9″ Handlebars are made from Post weld heat treated 4130 tubing with butted crossbar. These bars feature anti rust ED coating with masked knurling for better stem clamping.', 'BAR6', '1 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(7, '1', 'Khe', 'Khe Mvp Bar 9 Oil Slick', 'IMG_4737-300x300.jpg', 'IMG_4737.jpg', '', '', '', 400000, '-KHEbikes MVP handlebar\r\n-Colour: black\r\n-Weight: 736g\r\n-Multiple butted\r\n-Material: CrMo\r\n-Width: 736mm\r\n-Height: 9″/ 228,6mm\r\n-Backsweep: 11°\r\n-Upsweep: 2°\r\n-Clamping: 22,2mm', 'BAR7', '1 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(8, '1', 'Snafu', 'Snafu Jackson Bar', 'IMG_0165-300x300.jpg', 'IMG_0166.jpg', 'IMG_0168.jpg', 'IMG_0169.jpg', 'IMG_0165.jpg', 2000000, 'The Snafu Jackson bars are Scotty Cranmer’s signature 2-pc bars, constructed from 4130 heat-treated chromoly with a 20mm crossbar that tapers to 15mm in the center to help save weight in all the right areas without sacrificing strength. Rise: 9.25″ Width: 29″ Backsweep: 11° Upsweep: 1° Crossbar Height: 6.25″ Crossbar Width: 10.75″ Weight: 28.5 oz', 'BAR8', '1 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(9, '1', 'Fit-Bike', 'Fit Hoodbird Bar', 'IMG_1774-300x300.jpg', 'IMG_1774.jpg', 'IMG_1775.jpg', 'IMG_1776.jpg', 'IMG_1779.jpg', 1300000, 'Life time Warranty.', 'BAR9', '1 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(10, '1', 'Animal', 'Animal Foursome Bar', 'IMG_1546-300x300.jpg', 'IMG_1543.jpg', 'IMG_1544.jpg', 'IMG_1546.jpg', '', 1300000, '-Multi Butted Chromoly 4 pc Bars.\r\n-Post Weld Heat Treatment.\r\n-Rise: 9″\r\n-Width: 29″\r\n-Backsweep: 11°\r\n-Upsweep: 5°\r\n-Crossbar Height: 6″\r\n-Crossbar Width: 10″\r\n-Color – Black\r\n-LIFETIME WARRANTY\r\n-against breakage.', 'BAR10', '1 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(11, '1', 'S&M', 'S&M Credence XL Bar', 'IMG_1638-300x300.jpg', 'IMG_1638.jpg', 'IMG_1639.jpg', '', '', 1300000, 'RISE	        9.25″\r\nWIDTH	        30″\r\nUPSWEEP	        3°\r\nBACK SWEEP	11°\r\nCROSS BAR	5/8″\r\nWEIGHT	        2.25 lb (1.02 kg)\r\nMATERIAL	4130 CrMO', 'BAR11', '1 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(12, '1', 'S&M', 'S&M Elevenz Bars', 'IMG_1644-300x300.jpg', 'IMG_1644.jpg', 'IMG_1646.jpg', '', '', 1300000, 'RISE	        11″\r\nWIDTH	        30″\r\nUPSWEEP	        3°\r\nBACK SWEEP	11°\r\nCROSS BAR	5/8″\r\nWEIGHT	        2.25 lb (1.02 kg)\r\nMATERIAL	4130 CrMO', 'BAR12', '1 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(13, '1', 'Sunday', 'Sunday Street Sweeper 4PC Bars', 'IMG_0843-300x300.jpg', 'IMG_0844.jpg', 'IMG_0845.jpg', 'IMG_0846.jpg', '', 1350000, 'Jake Seeley is a tech street wizard, so he should have his very own bars to aid in the wizardry. We’re happy to introduce the Street Sweeper 4 piece bars.\r\n\r\nThese bars have a post-weld heat-treating which features a limited warranty against manufacturer’s defects.\r\n\r\nAvailable in Rust Proof Black only.\r\n    -Rise: 9” or 9.5″\r\n   -Width: 29”\r\n   -Upsweep: 3 degree\r\n   -Backsweep: 12 degree\r\n   -2lbs 3.0oz // 35oz // 2.188lbs // 992.25g', 'BAR13', '1 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(14, '2', 'Shadow', 'Shadow Linear Cable', 'IMG_4616-300x300.jpg', '', '', '', '', 200000, '-Linear style cable provides stiffer braking and resists kinking\r\n-5mm Teflon lined linear housing\r\n-50″ long\r\n-1.6mm Teflon coated inner wire\r\n-Alloy ferrules for durability', 'BREAK1', '1 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(15, '2', 'Dia-Compe', 'Dia-Compe Tech 77 Lever', 'IMG_4615-300x300.jpg', '', '', '', '', 350000, '• Diecast aluminum bracket (Black finish, Powder White)\r\n• Cold forged aluminum lever handle (Silver finish, Anodized, Powder white)\r\n• Hinged clamp\r\n• Freestyle use\r\n• 228g/pair', 'BREAK2', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(16, '2', 'Snafu', 'Snafu Anchor Lever', 'IMG_0118-300x300.jpg', '', '', '', '', 350000, 'New Snafu Anchor Lever', 'BREAK3', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(17, '2', 'Odyssey', 'Odyssey Gtx-S Gyro', 'IMG_0808-300x300.jpg', '', '', '', '', 600000, '6061 aluminum with precision sealed mechanism bearing and lower (17mm) stack height.\r\n\r\nAvailable in Anodized Black', 'BREAK4', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(18, '2', 'Odyssey', 'Odyssey Evo 2.5 Brake', 'IMG_0801-300x300.jpg', '', '', '', '', 800000, 'The Evo 2.5 is a remixed version of the industry leading Evo 2 brake you know and love, with a lot more room for larger, modern tire sizes. Just like the classic Evo 2, it is built around modular hardware features and affordable value. Install it as a rear brake or front brake straight out of the box.\r\n\r\nComes with Odyssey’s convenient double lugged straddle cable, custom curved cable hanger and an array of hardware for adapting the brake to every possible set-up. Forged from aluminum for strength, and to avoid unnecessary upcharges from frivolous CNC machining.\r\nFeatures\r\n\r\n    More clearance for larger tires\r\n    Flat-tipped cable set screws prevents excess damage to the cable’s inner wire\r\n    Sleek aluminum front cable adapter\r\n    Flush surfaces, with the lowest possible overall stack height\r\n    Front or rear wheel use\r\n    Slim by Four pads\r\n    Two sets of springs for hard and soft tension/modulation\r\n    Spring tension indicators for easy set up\r\n    Newly designed straddle hanger\r\n    Pre-lugged straddle cable\r\n    Cable lugs are smaller and use a 10 mm hex head', 'BREAK5', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(19, '2', 'Mission', 'Mission Cease V2 Brake', 'IMG_4609-300x300.jpg', '', '', '', '', 350000, 'PRODUCT RUNDOWN\r\nMade from industry-standard diecast aluminum, featuring extra-wide brake arm bends for large tire clearance and low-profile spring adjustment caps for universal functionality. Includes a simple one-piece straddle cable hanger system, brake pads, and all necessary hardware. The updated V2 design gives additional clearance, and the flat brake arm profiles eliminate any chance of brake arm rub.\r\n\r\nPRODUCT SPECIFICATIONS\r\nTYPE: 990 U-Brake\r\nHEIGHT: 23mm Stack Height\r\nMATERIAL: Die-Cast Aluminum\r\nINCLUDES: Brakes, Bolts, Pads, Straddle Cable\r\nWEIGHT: 5.9oz', 'BREAK6', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(20, '3', 'Shadow', 'Shadow Interlock V2 Chain', 'IMG_4961-300x300.jpg', '', '', '', '', 750000, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/5vfe1kg-Y-A\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n\r\n-The original half-link chain\r\n-1/8″ size, includes 98 links and 2 master pins\r\n-Colored chains are Teflon coated for low friction\r\n-Compatible with 8T driver and up\r\n-Weight: 11.3 oz for 72 links', 'CHAIN1', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(21, '3', 'Animal', 'Animal Hoder Chain', 'IMG_4676-300x300.jpg', '', '', '', '', 350000, 'The Mike Hoder signature 710 chain by KMC is designed with a heavy duty structure to stand up to the toughest street abuse. The design eliminates stretch and chain drop. The mushroomed riveting provides extra strength. Available in black with half-link. The only KMC 710 chain in black.', 'CHAIN2', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(22, '3', 'Cinema', 'Cinema Sync Chain', 'IMG_4673-300x300.jpg', '', '', '', '', 500000, 'Type: Half-Link Size: 1/8” Length: 85 Links Finish: Teflon Coated Weight: 13.6oz (11.5oz for 72 Links)\r\n\r\nThe Cinema Sync half-link chain offers complete adjustability without needing an additional link and the curved side plates reduce the chances of a broken chain caused by links being caught during missed grinds. Forged pins further increase the chain’s strength and the Teflon coating reduces friction and wear on both the chain and sprocket. Debossed Cinema logos on each link finish off the overall design and the pre-inserted connector pin makes for easy installation. Compatible with 8t drivers and up.', 'CHAIN3', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(23, '4', 'Odyssey', 'Odyyses Thunder Bolt + Crank Black', 'IMG_0807-300x300.jpg', '', '', '', '', 3000000, 'The Thunderbolt+ are Odyssey’s tried and true Thunderbolt cranks, along with new branding it also now it includes a Mid BB!\r\n\r\nThe Thunderbolt is one of the most technically advanced crank designs on the market. Due to continued advancements in our legendary 41-Thermal® processing and heat-treatment system, fatigue life has been increased by a massive margin.\r\n\r\nAvailable in Rust Proof Black and NEW Chrome.\r\n\r\nLearn About Odyssey’s Stampy Test\r\nFeatures\r\n\r\n    Laboratory tested and team proven\r\n    Updated open-ended spindle cap\r\n    Wedge cluster band with greater flexibility\r\n    Never-Wobble™ crank arm and spindle interface guarantee\r\n    41-Thermal® lifetime replacement warranty against bending, cracking and breaking\r\n\r\nSpecs\r\n\r\n    Traditional sprocket interface\r\n    165mm, 170mm, and 175mm\r\n    22mm Spindle\r\n    RHD or LHD\r\n    1lb. 11.8oz. (788g)\r\n    US Pat. Nos. 7,267,030\r\n    Odyssey 22mm Mid BB and spacers included', 'CRANK1', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(24, '4', 'Shadow', 'Shadow Stacked Mid BB 22MM', 'IMG_4592-300x300.jpg', '', '', '', '', 550000, '    100% CNC 6061-T6 alloy cones\r\n    Custom Shadow bearings with logo and red seals\r\n    Available in 22mm\r\n    Includes: 2 sealed bearings, internal spacer, custom cone spacers and alloy washers\r\n    Weight: 5.9 oz. for 19mm mid', 'CRANK2', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(25, '4', 'Animal', 'Animal Mid BB 22MM', 'IMG_4593-300x300.jpg', '', '', '', '', 450000, 'Animal Bottom Bracket Kit. Available in 22mm black', 'CRANK3', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(26, '4', 'Kink', 'Kink Mid BB 22MM', 'IMG_4596-300x300.jpg', '', '', '', '', 480000, 'Includes 2 Cone Spacers and 4 Washers', 'CRANK4', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(27, '5', 'Odyssey ', 'Odyssey R15 Fork', 'IMG_4744-300x300.jpg', '', '', '', '', 2500000, 'Our 41-Thermal® forks quickly established a reputation for strength and quality when we originally introduced them in 1999, and they have since gone on to change the standards that all forks are judged by. Our new R-Series forks continue this tradition.\r\nFeatures\r\n\r\n    Proven 41-Thermal® processed 4130 chromoly\r\n    Legendary lifetime replacement warranty\r\n    Laboratory tested and team proven\r\n    New, custom, seamless, tapered, butted and specially formed leg tube shaping for improved grind and guard clearance\r\n    All-new steerer tube design improves fatigue strength and includes a built-in, integrated, lower headset seat race\r\n    New, optimized, 4mm dropout shape for grind and peg clearance\r\n    Updated 7075-T6 pre-load bolt, US Pat. No. 7,591,474\r\n\r\nSpecs\r\n\r\n    15mm steeper, modern offset\r\n    165mm steerer tube\r\n    3/8” axle slots only\r\n    Starting at 1lb 15.2oz (885 grams)', 'FORK1', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(28, '5', 'Snafu', 'Snafu Magical Fork', 'IMG_0157-300x300.jpg', '', '', '', '', 3800000, 'The Magical fork is constructed from heat-treated chromoly for maximum strength featuring a 1-pc machined and reinforced steerer tube with integrated bearing race, alloy compression cap, tapered fork legs that are notched for peg clearance and 5mm thick solid dropouts to prevent bending. Height: 314mm, Steerer Tube Length: 164mm, Bearing Race: Integrated, Offset: 30mm, Weight: 30oz', 'FORK2', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(29, '5', 'Odyssey', 'Odyssey R25 Fork', 'IMG_0802-300x300.jpg', '', '', '', '', 2500000, 'Our 41-Thermal® forks quickly established a reputation for strength and quality when we originally introduced them in 1999, and they have since gone on to change the standards that all forks are judged by. Our new R-Series forks continue this tradition.\r\nFeatures\r\n\r\n    Proven 41-Thermal® processed 4130 chromoly\r\n    Legendary lifetime replacement warranty\r\n    Laboratory tested and team proven\r\n    New, custom, seamless, tapered, butted and specially formed leg tube shaping for improved grind and guard clearance\r\n    All-new steerer tube design improves fatigue strength and includes a built-in, integrated, lower headset seat race\r\n    New, optimized, 4mm dropout shape for grind and peg clearance\r\n    Updated 7075-T6 pre-load bolt, US Pat. No. 7,591,474\r\n\r\nSpecs\r\n\r\n    25mm steep offset\r\n    3/8” axle slots only\r\n    Starting at 1lb 15.2oz (885 grams)\r\n\r\n41-Thermal Lifetime Warranty', 'FORK3', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(30, '5', 'Fit-Bike', 'Fit-Bike V3 Fork', 'IMG_1649-300x300.jpg', '', '', '', '', 2100000, 'Our goal was to design and produce an extremely strong fork with plenty of tire clearance. After 2 years of R&D and a lot of investment in tooling, the Shiv V.3 is available with a quick 25mm offset. Here are a few features that make these forks so strong.\r\n\r\n    Using left and right specific investment cast drop-outs instead of traditional Cr-Mo plate, we’ve eliminated the weld gap on the inside of the drops, and were able to thicken up the drops toward the top making a much stronger drop-out/leg connection.\r\n    Seamless, swaged legs provide more strength than welded or fused caps-especially if pegs are installed. The ends of these legs are formed, not capped and welded so there is nothing to fracture.\r\n    A longer 166mm CNC’d steerer tube with increased wall thicknesses wherever possible and a 24mm x 1.5 compression cap with integrated race.\r\n    Tapered and butted legs provide tons of strength, crown weld surface, and peg clearance./li>\r\n    Post weld 4Q Baked heat-treatment\r\n\r\nSPECS >\r\n\r\n    Legs\r\n    Dropouts\r\n    Steerer Tube\r\n    Steerer Tube Length\r\n    Compression\r\n    Axle to Crown\r\n    Offset\r\n    Weight\r\n    Heat Treat\r\n\r\n    Seamless Swaged (No Welded Cap)\r\n    Investment Cast\r\n    1 Piece CNC-Machined 4130 CrMo\r\n    166mm\r\n    M24 X 1.5 Thread – 6mm Allen Slot\r\n    320mm\r\n    25mm\r\n    2.25 lb (1.02 kg)\r\n    4Q Baked Post-Weld', 'FORK4', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(31, '6', 'Animal', 'Animal SteamRoller Rim', 'IMG_1542-300x300.jpg', '', '', '', '', 1100000, 'Animal team tested. Brakeless design. 40mm Wide. 6061 aluminum construction. 36 spoke hole configuration. 389mm ERD (effective rim diameter). 18 ounces (per rim).', 'RIMS1', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(32, '6', 'Merrit', 'Merrit Battel Rims', 'IMG_0669-300x300.jpg', '', '', '', '', 1100000, 'our super strong Battle rim laced to our Non stop front hub. All wheels come with two free tension guards.', 'RIMS2', '', 0, NULL, '2020-11-17 17:00:00', NULL),
(33, '19', 'Terrible One', 'Terrible One SkapeGoat Farem 20,5', 'IMG_4785-300x300.jpg', 'IMG_4785.jpg', 'IMG_4785-600x450.jpg', '', '', 7500000, 'The Skapegoat frame from Terrible One. This frame is made from 100% 4130 chromoly tubing with a 75° head tube angle, 71° seat tube angle, 11.75″ bottom bracket height, 8.85″ standover height and a 13.35″ (slammed) chainstay length.\r\n\r\nThe Skapegoat frame benefits from an integrated head tube with drilled gyro tab holes, as well as top and down tube gussets for added strength.\r\n\r\nThe Skapegoat also features a Mid bottom bracket, an integrated seat post clamp, a Wishbone on the chainstay, clearance for 2.40″ tyres and 6mm thick dropouts with integrated chain tensioners to keep your wheel where it should be.', 'FRAME1', '8 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(34, '19', 'Kink Bmx', 'Kink Williams Frame 20,5', 'IMG_4760-300x300.jpg', 'IMG_4762.jpg', 'IMG_4761-600x450.jpg', 'IMG_4765-600x450.jpg', 'IMG_4768-600x450.jpg', 6500000, 'The Williams frame is Nathan Williams’ first signature frame from Kink and features modern street geometry while retaining a classic look. Despite the classic look though, the frame features several subtle details that should not be overlooked. The steeper 75.5-degree headtube is responsive, and the short 12.75” chainstay length makes the frame nimble without compromising on tire clearance; fitting up to 2.5” tires. The double butted toptube and downtube with gussets on both, ensure plenty of strength in the front end while the chainstay and seatstay bridge designs allot for generous tire clearance while adding as much stiffness as possible. Nathan’s frame of course comes standard with integrated seat post clamp, integrated chain tensioners, and removeable brake mounts. Additional details like the custom seatstay bridge badge inlay, and interior dropout knurling round out both the good looks and serious functionality of the Williams frame.The Williams frame is Nathan Williams’ first signature frame from Kink and features modern street geometry while retaining a classic look. Despite the classic look though, the frame features several subtle details that should not be overlooked. The steeper 75.5-degree headtube is responsive, and the short 12.75” chainstay length makes the frame nimble without compromising on tire clearance; fitting up to 2.5” tires. The double butted toptube and downtube with gussets on both, ensure plenty of strength in the front end while the chainstay and seatstay bridge designs allot for generous tire clearance while adding as much stiffness as possible. Nathan’s frame of course comes standard with integrated seat post clamp, integrated chain tensioners, and removeable brake mounts. Additional details like the custom seatstay bridge badge inlay, and interior dropout knurling round out both the good looks and serious functionality of the Williams frame.\r\nGeometry\r\nToptube Sizes	        20.5”\r\nChainstay Length	12.75” Slammed\r\nBottom Bracket Height	11.7″\r\nStandover Height	9″\r\nHeadtube Angle	        75.5°\r\nSeattube Angle	        71°\r\nTech Spec\r\nMaterial	        100% Seamless Sanko 4130 Chromoly Tubing\r\nHeadtube	Integrated Campy Spec\r\nDropouts	Investment Cast with Integrated Chain Tensioners\r\nBottom Bracket	Internally and Externally Machined Mid Size BB\r\nBraking	Removable Through-Bolt Brake Mount System\r\nParticulars	Toptube and Downtube Gussets\r\nOval Chainstays For Tire Clearance Up To 2.5”\r\nCNC’d Seatstay Bridge with Kink Badge Inlay\r\nAnti-Slip Interior Dropout Knurling\r\nWeight	4lb 15oz (20.75″)', 'FRAME2', '8 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(35, '19', 'Subrosa', 'Subrosa Code Frame 20,75', 'IMG_4778-300x300.jpg', 'IMG_4781-600x450.jpg', 'IMG_4779-600x450.jpg', 'IMG_4780-600x450.jpg', 'IMG_4784-600x450.jpg', 6000000, 'Subrosa CODE Frame is Lahsaan Kobza‘s signature street weapon.\r\n\r\nFrame material:	  4130 CrMo, TT and DT double butted\r\nSignature:	  Lahsaan Kobza\r\nTop tube:	  20.75″\r\nHead tube:	  75°\r\nSeat tube:	  69°\r\nChainstay:	  13.3” – 13.5”\r\nStandover:	  9”\r\nBB height:	  11.6”\r\nHeadset:	  Integrated headset\r\nGyro tabs holes:	  yes\r\nBrake hardware:	  completely removable, not included\r\nBottom Bracket:	  MID size\r\nSeat clamp:	  integrated\r\nDropout:	  6 mm laser cut for 14 mm axle\r\nFrame Weight:	  2.27 (20.5”) kg\r\nExtra:	          DT and TT bridge subrosa logo, investment cast wishbone, extended chainstay\r\nColour:	          Black', 'FRAME4', '8 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(36, '19', 'Kink Bmx', 'Kink Cloud Frame 20,5', 'IMG_4770-300x300.jpg', 'IMG_4771-600x450.jpg', 'IMG_4772-600x450.jpg', 'IMG_4774-600x450.jpg', 'IMG_4776-600x450.jpg', 5800000, 'ravis Hughes’s first signature frame, the Kink Cloud. The Cloud is really about function over form, a no-frills, straight to the point, super-strong frame that is meant to be ridden. You won’t find any elaborate bridge designs or complex casting shapes, just clean lines, 100% chromoly tubing, gussets top and bottom, and clean laser-cut dropouts. Of course, the Cloud frame still includes the convenient features you come to expect from Kink like the integrated chain tensioners and seat post clamp. Most importantly, as anyone who’s seen Travis ride already knows, the Cloud frame can take a beating.\r\n\r\nColor : Matte Strom Blue\r\n\r\nGeometry\r\nToptube Sizes	        20.5”\r\nChainstay Length	13.5” Slammed\r\nBottom Bracket Height	11.65″\r\nStandover Height	8.75″\r\nHeadtube Angle	        75.5°\r\nSeattube Angle	        71°\r\n\r\nTech Spec\r\nMaterial	100% 4130 Chromoly Tubing\r\nHeadtube	Integrated Campy Spec\r\nDropouts	Laser Cut with Integrated Chain Tensioners\r\nBottom Bracket	Internally and Externally Machined Mid Size BB\r\nBraking	Brakeless\r\nParticulars	Toptube and Downtube Gussets\r\nIntegrated Chain Tensioners\r\nIntegrated Seat Clamp\r\nClean Lines\r\nWeight	5lb 4oz (20.5″)', 'FRAME4', '8 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(37, '19', 'Subrosa', 'Subrosa Om Frame 20,75', 'IMG_4292-300x300.jpg', 'IMG_4294-600x450.jpg', 'IMG_4293-600x450.jpg', 'IMG_4296-600x450.jpg', 'IMG_4298.jpg', 6000000, 'Utilizing everything that matters and nothing that doesn’t was the running theme of the Subrosa OM frame. Removing expensive flare such as welded on chain and seat stay caps, excluding an integrated seat clamp, and paying close attention to other solely aesthetic design features allowed us to develop an amazing, high quality after market frame with the best possible value we could provide.\r\n\r\n \r\n\r\nJoris’ riding style is progressive and unique. To compliment this he wanted a special frame that would allow his creative riding to expand. Utilizing a 11.7″ bottom bracket height for clearance, 12.9″ slammed rear end, and a taller 125mm head tube this frame was designed to Joris’ exact specifications so he can continue doing what he does best…Killing It!', 'FRAME5', '8 Kg', 0, NULL, '2020-11-17 17:00:00', NULL),
(38, '19', 'Snafu', 'Hyper Indy Frame 20,5', 'IMG_0176-300x300.jpg', 'IMG_0177.jpg', 'IMG_0178.jpg', '', '', 9000000, 'Logan Martin Signature Frame<br>\r\n\r\n<br>Top tube: 20.5”<br>\r\n<br>Backend Length: 13.2″ slammed<br>\r\n<br>Head-tube Angle: 75 degree<br>\r\n<br>Seat-tube angle: 71 degree<br>\r\n<br>BB Height: 11.625″<br>\r\n<br>BB style: Mid<br>\r\n<br>Axle size: 14mm<br>\r\n<br>Brake Mounts: Removable<br>\r\n<br>Gyro Tabs: Removable (pre-drilled)<br>\r\n<br>Frame weights: 4 lb 5 oz<br>', 'FRAME6', '8 Kg', 0, NULL, '2020-11-18 11:51:48', NULL),
(39, '19', 'Snafu', 'Hyper Wizard Frame 20,4', 'IMG_0185-300x300.jpg', 'IMG_0187.jpg', 'IMG_0189.jpg', '', '', 9000000, '<br>Top tube: 20.4″<br>\r\n<br>Backend Length: 13.2″ slammed<br>\r\n<br>Head-tube Angle: 75 degree<br>\r\n<br>Seat-tube angle: 71 degree<br>\r\n<br>BB Height: 11.8″<br>\r\n<br>BB style: Mid<br>\r\n<br>Axle size: 14mm<br>\r\n<br>Brake Mounts: Removable<br>\r\n<br>Gyro Tabs: Removable (pre-drilled)<br>\r\n<br>Frame weights:\r\n<br>20.4″- 4 lb 5 oz<br>', 'FRAME7', '8 Kg', 0, NULL, '2020-11-18 11:50:48', NULL),
(40, '19', 'S&M', 'S&M C.C.R Frame 21', 'IMG_1655-300x300.jpg', 'IMG_1658.jpg', '', '', '', 8500000, '<br>The Credence Clint Reynolds – a.k.a.<br> <br>the C.C.R. – is, you guessed it, Clint<br>\r\n<br>Reynolds’ signature sled.<br>\r\n<br>TOP TUBE LENGTHS 21″, 21.25″,21.5″<br>\r\n<br>REAR END LENGTH 13.9″<br>\r\n<br>HEAD TUBE ANGLE 74.25°<br>\r\n<br>STANDOVER HEIGHT 9″<br>\r\n<br>B.B. HEIGHT 11.61″<br>\r\n<br>SEAT TUBE ANGLE 71°<br>\r\n<br>HEAD TUBE Integrated Campy Style<br>\r\n<br>B.B. STYLE Mid<br>\r\n<br>BRAKE MOUNTS Chainstay Welded-On<br>\r\n<br>DROPOUTS 3/8″<br>\r\n<br>SEAT POST SIZE 1″ / 25.4mm<br>\r\n<br>WEIGHT 5.25 lb<br>\r\n<br>MADE IN USA<br>', 'FRAME8', '8 Kg', 0, NULL, '2020-11-18 11:52:52', NULL),
(41, '20', 'BUILD IN MIC RIDE', 'BUILD IN MIC RIDE', 'IMG_0009.jpg', 'IMG_0015.jpg', 'IMG_0018-1.jpg', 'IMG_0025.jpg', 'IMG_0022-1.jpg', 25000000, '', 'COMPLETE1', '18 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(42, '20', 'KHE - BIKES', 'KHE SILINCER', 'IMG_1065-300x300.jpg', 'IMG_1069.jpg', 'IMG_1075.jpg', 'IMG_1068.jpg', 'IMG_1071.jpg', 8000000, '', '', '20 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(43, '20', 'KHE-BIKES', 'KHE BARCODE 2019', 'IMG_1111-300x300.jpg', 'IMG_1119.jpg', 'IMG_1121.jpg', '', 'IMG_1113.jpg', 8000000, '', 'COMPLETE2', '20 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(44, '21', 'Shadow', 'Shadow Revive Ankle Support', 'IMG_4967-300x300.jpg', 'IMG_4967-300x300.jpg', '103-06024_tsc_reviveanklesupport_onfoot-inside_web.jpg', '103-06024_tsc_reviveanklesupport_onfoot-outside_web.jpg', '', 600000, 'Sold Individually<br>\r\n\r\nThe Shadow Revive Ankle Support gives you the tool needed for support, recovery and to keep going after an injury. Constructed of 4-way stretch elastic material that is engineered for increased compression and fit without heat retention. The Stabalite buttresses, along with the wrap around figure-8 strap design provide added lateral stability, support and comfort. The low profile design allows a clean fit inside shoes. Quick and easy to put on and with tech features that let you get back at it with comfortable stabilization.<br>\r\n\r\nSpec : <br>\r\n    Sold Individually<br>\r\n    Fits left or right<br>\r\n    4-way stretch elastic<br\r\n    Stabalite buttress for support<br\r\n    Figure 8 hook and loop straps<br>\r\n    Size One size fits most<br>', 'SAFETY1', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(45, '21', 'Shadow', 'Shadow Invisa-Lite Shin / Angkle Combo', 'IMG_4968-300x300.jpg', 'IMG_4968.jpg', '103-06033_tsc_invisa-lite_shin-anklecombo_front_web.jpg', '103-06033_tsc_invisa-lite_shin-anklecombo_isoonleg_web.jpg', '', 1200000, 'Sold In Pairs<br>\r\n\r\nThe Invisa-Lite Shin / Ankle Combo Pad is constructed from a breathable neoprene with a deluxe lining for maximum comfort. Custom designed and articulated high durometer 3D impact foam keeps shins safe. We use a 4 way stretch ankle &lsquo;sock&rsquo; paired with a medium durometer 3D impact foam to keep ankles protected. One size fits most with an open back design, paired with a heavy-duty adjustable hook and loop closure.<br>\r\n\r\nSpec : <br>\r\n\r\nSold In Pairs<br>\r\n3D impact foam shin and ankle pads<br>\r\nBreathable, lightweight neoprene body<br>\r\nSturdy 4 way stretch ankle sleeve with stirrup<br>\r\nDouble and single needle heavy duty stitching<br>\r\nSize: One Size Fits Most', 'SAFETY2', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(46, '21', 'Bell', 'Bell Nitro Circus Helmet', 'IMG_4636-300x300.jpg', '', '', '', '', 1000000, 'Now you can own the same helmets the Nitro Circus athletes wear. The Local Helmet from Bell offers classic skate style, combined with new-school technology. In the dirt or out on the street, this helmet is comfortable and cool.\r\n\r\nCERTIFICATIONS: CPSC Bicycle, CE EN1078 | ASTM F1492-08 (Skate)\r\nCONSTRUCTION: ABS Hard Plastic Shell with EPS Liner\r\nWEIGHT: 453 grams (size M)\r\nVENTS: 10\r\nCOLOR: Nitro Circus Gloss Silver/Blue/Red\r\n\r\nSIZES:\r\nM  55–59 cm\r\nL  59–61.5 cm', 'SAFETY3', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(47, '21', 'Shadow', 'Shadow Gloves (SIZE M)', 'IMG_4618-300x300.jpg', '', '', '', '', 500000, 'The Shadow Conspire Glove is our ultimate glove, combining all the tech, materials and design that have been the hallmark of Shadow Riding Gear. Built around lightweight synthetic leather construction with ultimate flexibility and feel, the Conspire Gloves also have a silicone palm print for maximum grip control. The Slim Tech Cuff, featuring a minimal hook and loop closure, Coffin integrated pull tab, and stretch neoprene, all work together to give the Conspire Glove the look and feel of a slip on glove with added security and adjustability of traditional closure gloves. All of these features work to keep your hands firmly on the grips, keeping the session going, ultimately extending the ride.\r\n#feeltheride #extendtheride', 'SAFETY4', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(57, '7', 'Subrosa', 'SUbrosa Griffin Grips', 'IMG_4964-300x300.jpg', '', '', '', '', 230000, 'Mark has put in so many hours holding onto his bike, that when it came to designing his new signature Griffin Grip it was pretty easy for him. One flat side, and one small flanged side gives riders more options for comfort and personal preference. Soft, durable DCR formula increases the life of the grip, and feels great from the first ride on.\r\n\r\nMark Burnett Signature Grip\r\n\r\n    Proven DCR material\r\n    170mm length\r\n    Includes Subrosa Nylon bar ends', 'GRIPS1', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(58, '7', 'Shadow', 'Shadow Ol\'Dirty Grip', 'IMG_4963-300x300.jpg', '', '', '', '', 230000, 'Byron Anderson is an OG Shadow rider and product designer extraordinaire. Back in the day he designed the Shadow Ol’ Dirty Grip as his signature grip and it was one of our most popular grips. So we got together with Byron and have brought it back with updated features and constructed with our durable and soft DCR material. Opposing rib cells provide excellent feel, durability and grip. Guaranteed to be your favorite grip.\r\n\r\n    Flangeless symmetrical design\r\n    Proprietary DCR rubber\r\n    160mm length\r\n    Shadow nylon bar ends included', 'GRIPS2', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(59, '7', 'Shadow ', 'Shadow VVS Grips', 'IMG_4962-300x300.jpg', '', '', '', '', 230000, 'Before adding this bike to your shopping cart, we encourage you to click the button above and check to see if you have a Subrosa dealer near you. When you purchase a bike from your local bike shop, you’ll get a bike that is professionally assembled and serviced. Also, by supporting your local bike shop, you’re helping build your local scene. If you don’t have a dealer near you, please proceed and we’ll ship your new bike right to your door.\r\n\r\nMatt Ray signature grip with classic ribbed design and the usual Shadow ultra-details. Great hand feel and durable. For high end, performance grippage, go VVS\r\n\r\nSpec:\r\n\r\n    Micro flange ribbed design\r\n    Proprietary DCR rubber\r\n    165mm length\r\n    Shadow nylon bar ends', 'GRIPS3', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(60, '7', 'ODI', 'Odi Alumunium End Plugs', 'IMG_4644-300x300.jpg', '', '', '', '', 350000, 'Protect your bars and grips with ODI Aluminum End Plugs. Constructed of sturdy 6061 aluminum, these plugs are designed to take the brunt of whatever you can throw at them. Available in anodized black or silver.\r\n\r\nPRODUCT FEATURES:\r\n\r\n    Sturdy 6061 Aluminum\r\n    Fits most sized bars\r\n    Triple-Core Design Holds Plugs Inside Bars\r\n    Sold in pairs', 'GRIPS4', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(61, '7', 'Subrosa', 'Subrosa Bitchin Barends', 'IMG_4643-300x300.jpg', '', '', '', '', 350000, 'Laser etched logo 7075 alloy, CNC machined\r\nExpansion style wedge fits most bars cut or uncut\r\nWeight: 0.75 oz.', 'GRIPS5', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(62, '7', 'Shadow', 'Shadow Ol\'Dirty Grips', 'IMG_4571-300x300.jpg', '', '', '', '', 220000, 'Byron Anderson is an OG Shadow rider and product designer extraordinaire. Back in the day he designed the Shadow Ol’ Dirty Grip as his signature grip and it was one of our most popular grips. So we got together with Byron and have brought it back with updated features and constructed with our durable and soft DCR material. Opposing rib cells provide excellent feel, durability and grip. Guaranteed to be your favorite grip', 'GRIPS6', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(63, '7', 'Terribe One', 'Terrible One Grips', 'IMG_4560-300x300.jpg', '', '', '', '', 220000, 'Joe Rich designed the T-1 grips to be thick in the spots that wear out first, a little thinner where they don’t\r\n145mm length\r\ninclude rubber Coffee Cup bar ends', 'GRIPS7', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(64, '7', 'S&M', 'S&M Passero Grips', 'IMG_1771-300x300.jpg', '', '', '', '', 250000, 'Craig’s flangeless signature grip boasts two distinct patterns, half cushy micro-ribbed shield and half sturdy triangle pattern, so you can run ‘em your way. Made in the USA by ODI.\r\n\r\nDiameter: 30mm\r\nLength: 163mm\r\nCompound: Soft\r\nBar Ends: Nylon Plug Style w/ Sharpie Shield Logo', 'GRIPS8', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(65, '7', 'Animal', 'Animal Edwin V2 Grips', 'IMG_2170-300x300.jpg', '', '', '', '', 200000, 'The new version of the classic Edwin DeLarosa flangeless signature grip. The grips are designed with a wavy mushroom style pattern that is soft and comfortable. They also come with a matching pair of our plastic sewer cap bar ends. 165mm Weight – 4.0 oz (pair)', 'GRIPS9', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(66, '8', 'BSD', 'BSD Highriser Headset', 'IMG_4598-300x300.jpg', '', '', '', '', 450000, 'Taller version of our integrated headset, comes with top and bottom sealed bearings, crown race, compression washer, BSD logo top cap and some thin washers for fine adjustment', 'HS1', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(67, '8', 'WTP', 'WTP Compact Headset', 'IMG_4604-300x300.jpg', '', '', '', '', 400000, 'MATERIAL:\r\n6061 alloy topcap with integrated spacer\r\n\r\nSIZE:\r\nprecision high quality bearings for use with integrated headtubes\r\n\r\nSPECIAL FEATURES:\r\n\r\nincl. all necessary washers / bearings / spacers, etc.\r\n\r\n16mm topcap\r\n\r\nincludes x2 3mm spacers\r\n\r\nCOLOR:\r\nblack, silver polished, red\r\n\r\nWEIGHT:\r\n\r\n104g (3.7oz)\r\n\r\n', 'HS2', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(68, '8', 'Eclat', 'Eclat Wave Headset', 'IMG_4603-300x300.jpg', '', '', '', '', 400000, 'Material\r\n6061-T6 alloy cnc machined, high-end sealed bearings\r\n\r\nColours\r\nblack\r\n\r\nFeatures\r\nmid-stack 16mm cap, variable stack heights between 16-22mm possible.\r\n\r\nWeight\r\n65g (2.3oz) (includes everything)', 'HS3', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(69, '8', 'Merrit', 'Merrit Hight Top Headset', 'IMG_4602-300x300.jpg', '', '', '', '', 450000, 'The Hightop Headset is a Campy spec integrated headset with a 20mm tall CNC?d 6061 aluminum cap with laser engraved logos, two sealed bearings and an aluminum compression ring. Keeps things simple and clean by removing the need for extra headset spacers..', 'HS4', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(70, '8', 'IMG_1770-300x300.jpg', 'Sunday', 'IMG_1770-300x300.jpg', '', '', '', '', 350000, 'New dust cap design, with Sunday laser etched logo, for a cleaner look to accommodate a taller riding set up with less spacers.\r\n\r\nThe Sunday BMX headset is designed to be a quality headset at an affordable price.\r\nFeatures\r\n\r\n    New dust cap design\r\n    Tall stack height (12mm)\r\n    Matching spacers\r\n    Laser etched Sunday logo\r\n    1 1/8″ integrated\r\n    Precision tapered wedge to keep headset tight\r\n    Compatible with Odyssey Gyros and Snafu Mobeus\r\n\r\nSpecs\r\n\r\n    Campagnolo standard spec\r\n    Outer Diameter: 41.8mm\r\n    Bearings: 45º tapered\r\n    Weight w/out Spacers: 2.1 oz.\r\n    Weight with Spacers: 2.3 oz.', 'HS5', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(71, '8', 'Odyssey', 'Odyssey Conical Pro Headset', 'IMG_0792-300x300.jpg', '', '', '', '', 450000, 'New dust cap design, with Monogram laser etched logo, for a cleaner look to accommodate a taller riding set up with less spacers. Same quality as our Pro Headset using high-quality sealed bearings and a precision tapered wedge to keep the headset tight at all times.\r\n\r\nAvailable in Black, High Polished, and White.\r\n\r\n    FEATURES\r\n    High-quality sealed bearings.\r\n    Color matched spacers included.\r\n    15mm dust cap.  With 3mm, and 2mm spacers\r\n    Laser etched Odyssey bearings.\r\n\r\n    SPECS\r\n    Campagnolo standard spec. 1-1/8 integrated headset.\r\n    45º tapered bearings\r\n    41.8mm outer diameter (fits 42mm BMX head tubes).', 'HS6', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(72, '8', 'Animal', 'Animal Skyline Headset', 'IMG_4608-300x300.jpg', '', '', '', '', 480000, 'The Animal Skyline Headset is Campagnolo-compatible integrated headset. Includes tall and standard-size aluminum upper dust covers with internal O-rings to keep dirt and grime out. Laser etched Animal logos for a clean look. Includes 5mm and 7mm headset spacers. Available in black.', 'HS7', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(73, '9', 'Odyssey', 'Odyssey Hub Set Rain Purple (LIMITED EDITION)', 'IMG_4660-300x300.jpg', 'IMG_4662.jpg', '', '', '', 4500000, 'Clutch v2 freecoaster hub RHD and Vandero pro front hub<br>\r\n\r\n<h3>Back by popular demand – Purple Rain!</h3>\r\n\r\n<br>We originally did “purple rain” back in 2008, and it has been one of the most requested finishes to return over the years. We are happy to say that it is back again for a limited time on select products, so be sure to stock up accordingly as its sure to be well received.\r\n\r\nThe Clutch Hub is a traditional clutch-based coaster hub design that eliminates all of the problematic elements on ordinary hubs and only keeps the parts that work. A new, super-strong FULL 14mm axle, bearing configuration, and drag mechanism design stops the need for constant repairs. Equipped with external slack adjustment, which can be done with a 2.5mm hex key through a hub shell access hole (can be adjusted without removing the wheel), and an improved plastic hub guard spec.\r\n\r\nSame Clutch coaster hub you’ve come to love, but with a few upgrades.</br>', 'HUBS1', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(74, '9', 'Odyssey', 'Odyssey C5 Cassete Hub', 'IMG_4658-300x300.jpg', 'IMG_4659.jpg', '', '', '', 2000000, 'The C5 is a strong, drive switchable, affordably priced cassette hub with a 14mm chromoly male axle. The LHD/RHD compatible 9T driver rolls on three precision, sealed cartridge bearings. A replaceable plastic hub guard is included.\r\n\r\nKey Features\r\n\r\n    36-H, 6061 T-6 aluminum hub shell\r\n    14mm, 4130 chromoly hollow axle, w/6mm hex broaching\r\n    9T driver with 3-Pawl independent spring design\r\n    RHD/LHD switchable\r\n    Removable plastic hub guards (compatible with Odyssey/G-Sport hubs)\r\n    6802 sealed cartridge driver bearings (3)\r\n    6903 sealed cartridge hub shell bearings (2)\r\nSpecs\r\n\r\nDrive Side Effective Flange Diameter: 49mm\r\nNon-Drive Side Effective Flange Diameter: 49mm\r\nDrive Side Flange to Center Distance: 27mm\r\nNon-Drive Side Flange to Center Distance: 30mm', 'HUBS2', '1 Kg', 0, NULL, '2020-11-18 17:00:00', NULL),
(75, '10', 'Shadow', 'Shadow Ravager Plastic Pedal', 'IMG_4965-300x300.jpg', '', '', '', '', 350000, '    Low profile body with knurling for additional traction\r\n    Injection molded composite with molded pins\r\n    Loose ball only\r\n    4140 heat treated spindle\r\n    Replacement parts available\r\n    Weight: 13.6 oz per pair', 'PEDALS1', '1 Kg', 0, NULL, '2020-11-25 17:00:00', NULL),
(76, '10', 'FIT BIKE', 'Fit Mac PC Pedals', 'IMG_1709-300x300.jpg', '', '', '', '', 300000, 'Nylon version of our popular MAC pedal. Oversized CrMo spindle for strength. The ergonomic shape is designed to grip your foot better than traditional pedal shapes. Micro-knurled surface and molded pins for traction.', 'PEDALS2', '1 Kg', 0, NULL, '2020-11-25 17:00:00', NULL),
(77, '10', 'MERRIT', 'Merrit P1 Pedals', 'IMG_0672-300x300.jpg', '', '', '', '', 300000, 'The P1 Pedals have a low profile design that uses a special nylon/fiberglass blend that increases strength and extends pedal life.\r\nWeight: 12.9 oz.', 'PEDALS3', '1 Kg', 0, NULL, '2020-11-25 17:00:00', NULL),
(78, '11', 'Mission', 'Mission Targa Peg', 'IMG_4646-300x300.jpg', '', '', '', '', 280000, 'Length: 4.5″\r\nDiameter: 40mm\r\nDurable and Replaceable Nylon Sleeve\r\nSteel Inner Peg Core\r\n14mm with 3/8″ Adapter Included\r\nSold Individually\r\nReplacement Sleeves Sold Separately\r\nWeight Per Peg: 9.0oz', 'PEGS1', '1 kg', 0, NULL, '2020-11-25 17:00:00', NULL),
(79, '11', 'Cinema', 'Cinema S45 Peg', 'IMG_4647-300x300.jpg', '', '', '', '', 300000, 'The S45 peg is a 4.5” long, heat treated SCM415 chromoly steel peg, with internal butting and external reliefs to reduce weight without sacrificing strength. The forging process makes the S45 super strong and lasting, and the final CNC machining keeps the weight down and creates a smooth finish. Sold individually, 14mm and includes 10mm (3/8”) adapter.', 'PEGS2', '1 Kg', 0, NULL, '2020-11-25 17:00:00', NULL),
(80, '11', 'Animal', 'Animal Team Peg', 'IMG_4645-300x300.jpg', '', '', '', '', 300000, 'Nothing beats the raw sound and feel of steel. The Animal Team Peg is a fresh addition to the legacy of Animal pegs. The Team Pegs measure in at 4.5″ inches long for full contact. Forged, heat-treated chromoly steel for strength and smooth grinding on all surfaces. Internally butted, so they are thicker where it counts and lighter overall. Drilled for 14mm axles, with a 3/8 adapter included.', 'PEGS3', '1 Kg', 0, NULL, '2020-11-25 17:00:00', NULL),
(81, '11', 'FIT BIKE', 'Fit O.G Pegs', 'IMG_1740-300x300.jpg', '', '', '', '', 300000, 'Keep it real with steel! Our O.G. Pegs are super strong and light thanks to butted and heat-treated 4130 CrMo. Nothing beats the sound and feel of grinding or jibbing with steel pegs!\r\n\r\n    Material: 4Q Baked 4130 CrMo\r\n    Butted to save weight with thicker base and end for longer life\r\n    Diameter: 34mm\r\n    Three Lengths: 112mm/4.4″ (6.25oz.)\r\n    14mm with 3/8″ adaptor.\r\n    5 threaded anti-roll hole positions (bolt included)\r\n    Black only\r\n    Sold individually', 'PEGS4', '1 Kg', 0, NULL, '2020-11-25 17:00:00', NULL),
(82, '11', 'Merrit', 'Merrit Brandon Begin GFE Pegs', 'IMG_0689-300x300.jpg', '', '', '', '', 330000, 'Brandon’s signature GFE plastic pegs use a 4140 steel core to keep the size down and allow for thicker stronger plastic. Each peg sleeve has a steel washer molded in to keep your peg in place. Available in 4.75 and 4.25. 14mm with spacer. !!! SOLD INDIVIDUALLY!!!.\r\nGFE peg weight: 6.5 oz.\r\nGFE peg sleeve weight: 2.1 oz.', 'PEGS5', '1 Kg', 0, NULL, '2020-11-25 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Pikih', 'pikiha52@gmail.com', NULL, '$2y$10$fFwTgQ02WKMCm8NfVDELB.3De89HNjEEjs5WCw0ccS4y7EDVJiAAa', '5gyTlU8k8YCiSyJbcYOUrFGmyO6dKYSB9UShu3bkGhJvqsP0anpeBZN6orx8', '2020-11-17 23:13:47', '2020-11-17 23:13:47');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indeks untuk tabel `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `complete_bikes`
--
ALTER TABLE `complete_bikes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `parts`
--
ALTER TABLE `parts`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `complete_bikes`
--
ALTER TABLE `complete_bikes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `parts`
--
ALTER TABLE `parts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
