<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\BmxController;
use App\Http\Controllers\BrandsController;
use App\Http\Controllers\CompletebikesController;
use App\Http\Controllers\PartsController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BlogController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route:: get('/', [BmxController::class, 'index']);
Route:: get('/info', [BmxController::class, 'info']);

Route:: get('/login', [AuthController::class, 'index']);
Route:: post('login', [AuthController::class, 'login']);

Route:: get('/register', [AuthController::class, 'Showregister']);
Route:: get('register', [AuthController::class, 'register']);

Route::group(['middleware' => 'auth'], function () {

    Route:: get('logout', [AuthController::class, 'logout']);

});


Route:: get('/brands', [BrandsController::class, 'index']);

Route:: get('/completebikes', [CompletebikesController::class, 'index']);

Route:: get('/blog', [BlogController::class, 'index']);
Route::get('/blog/{id}', 'BlogController@show')->name('blog.detail');

//menampilkan product
Route:: get('/parts', [PartsController::class, 'index']);
Route:: get('/parts/bars', [PartsController::class, 'bars']);
Route:: get('/parts/breaking', [PartsController::class, 'breaking']);
Route:: get('/parts/chains', [PartsController::class, 'chains']);
Route:: get('/parts/forks', [PartsController::class, 'forks']);
Route:: get('/parts/cranks', [PartsController::class, 'cranks']);
Route:: get('/parts/rims', [PartsController::class, 'rims']);
Route:: get('/parts/frame', [PartsController::class, 'frame']);
Route:: get('/parts/completebikes', [PartsController::class, 'completebikes']);
Route:: get('/parts/safetygear', [PartsController::class, 'safetygear']);
Route:: get('/parts/grips', [PartsController::class, 'grips']);
Route:: get('/parts/headset', [PartsController::class, 'headset']);
Route:: get('/parts/hubs', [PartsController::class, 'hubs']);
Route:: get('/parts/hubs', [PartsController::class, 'hubs']);
Route:: get('/parts/pedals', [PartsController::class, 'pedals']);
Route:: get('/parts/pegs', [PartsController::class, 'pegs']);
Route:: get('/parts/seat', [PartsController::class, 'seat']);
Route:: get('/parts/smallparts', [PartsController::class, 'smallparts']);
Route:: get('/parts/spokes', [PartsController::class, 'spokes']);
Route:: get('/parts/sprockets', [PartsController::class, 'sprockets']);
Route:: get('/parts/stems', [PartsController::class, 'stems']);
Route:: get('/parts/tires', [PartsController::class, 'tires']);
Route:: get('/parts/tools', [PartsController::class, 'tools']);
Route::get('/parts/{id}', 'PartsController@show')->name('parts.show');

//

Route::post('cart', 'CartController@addToCart')->name('parts.cart');
Route::get('/cart', 'CartController@listCart')->name('cart.list_cart');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//admin

Route::get('/admin', [AdminController::class, 'index']);

