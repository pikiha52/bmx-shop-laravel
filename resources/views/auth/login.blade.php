@extends('layout.header')

@section('title', 'Mic Ride - Login here')

@section('container')

<section class="my_account_area pt--80 pb--55 bg--white">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-12">
						<div class="my__account__wrapper">

							<h3 class="account__title">Login</h3>
							<form class="login100-form validate-form p-b-33 p-t-5" method="post" action="{{ route('login') }}">
                                @csrf

								<div class="account__form">
									<div class="input__box">
										<label>{{ __('E-Mail Address') }} <span>*</span></label>
										<input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                       
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
									<div class="input__box">
										<label>{{ __('Password') }}<span>*</span></label>
										<input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror" required autocomplete="current-password">
                                    
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
									<div class="form__btn">
										<button type="submit">{{ __('Login') }}</button>
										<label class="label-for-checkbox">
											<input id="rememberme" class="input-checkbox" name="remember" value="forever" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
											<span>{{ __('Remember Me') }}</span>
										</label>
                                    </div>
                                    @if (Route::has('password.request'))
                                    <a class="forget_pass" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                                    @endif
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>

@endsection
