@extends('layout/header')

@section('title', 'Mic Ride - Parts')

@section('container')

<!-- Start Shop Page -->
<br>
<br>
<br>
<section class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>{{ $h3 }}</h3>
                <div class="tab__container">
                    <div class="shop-grid tab-pane fade show active" id="nav-grid" role="tabpanel">
                        <div class="row">
                            @foreach( $parts as $part)
                            <!-- Start Single Product -->
                            <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                                <div class="product">
                                    <div class="product__thumb">
                                        <a class="first__img" href="{{ url('parts/' . $part->id) }}"><img
                                                src="/assets/images/products/{{ $part['images'] }}" height="300" width="300"
                                                alt="product image"></a>
                                        <ul class="prize position__right__bottom d-flex">
                                            <li>Rp {{ $part -> price }}</li>
                                        </ul>
                                        <div class="action">
                                            <div class="actions_inner">
                                                <ul class="add_to_links">
                                                    <li><a class="cart" href="cart.html"></a></li>
                                                    <li><a class="wishlist" href="wishlist.html"></a></li>
                                                    <li><a class="compare" href="compare.html"></a></li>
                                                    <li><a data-toggle="modal" title="Quick View"
                                                            class="quickview modal-view detail-link"
                                                            href="#productmodal"></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product__content">
                                        <h4><a href="{{ url('parts/' . $part->id) }}">{{ $part -> merk }}</a></h4>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection