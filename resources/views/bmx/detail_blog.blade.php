@extends('layout.header')

@section('title', 'Mic Ride - News')

@section('container')

 <!-- End Bradcaump area -->
 <section class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
 <div class="page-blog-details section-padding--lg bg--white">
			<div class="container">
				<div class="row">
					<div class="col-lg-9 col-12">
						<div class="blog-details content">
							<article class="blog-post-details">
								<div class="post-thumbnail">
									<img src="/assets/images/blog/blog-3/{{ $blog['gambar'] }}" alt="blog images">
								</div>
								<div class="post_wrapper">
									<div class="post_header">
										<h2>{{ $blog->judul }}</h2>
										<ul class="post_author">
											<li>Posts by : <a href="#">Admin</a></li>
											<li class="post-separator">/</li>
											<li>{{ $blog->created_at }}</li>
										</ul>
									</div>
									<div class="post_content">
										<p>{{ $blog->isi }}</p>
										<div class="post-thumbnail">
											<img src="/assets/images/blog/blog-3/{{ $blog['url'] }}" alt="blog images">
										</div>
									</div>
									<div class="post_content">
										<p>{{ $blog->isi2 }}</p>
									</div>

									<ul class="blog_meta">
										<li>Tags:<span>{{ $blog->tag }}</span></li>
									</ul>
									<ul class="social__net--4 d-flex justify-content-start">
										<li><a href="#"><i class="zmdi zmdi-rss"></i></a></li>
										<li><a href="#"><i class="zmdi zmdi-linkedin"></i></a></li>
										<li><a href="#"><i class="zmdi zmdi-vimeo"></i></a></li>
										<li><a href="#"><i class="zmdi zmdi-tumblr"></i></a></li>
										<li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
									</ul>
								</div>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
 </section>

@endsection