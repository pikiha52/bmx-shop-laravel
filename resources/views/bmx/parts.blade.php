  @extends('layout/header')

  @section('title', 'Mic Ride - All Parts')

  @section('container')
  <!-- Main Container -->
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <section class="main-container col1-layout bounceInUp animated">
  	<div class="main container">
  		<div class="container">
  			<div class="row">
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/bars') }}"><img src="{{ url('assets/images/icons/bars.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Bars</h6>
  				</div>
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/breaking/') }}"><img src="{{ url('assets/images/icons/brakes.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Braking</h6>  
				</div>
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/chains') }}"><img src="{{ url('assets/images/icons/chain.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Chains</h6>  
				</div>
				  <div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/cranks/') }}"><img src="{{ url('assets/images/icons/cranks-1.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Cranks & BB</h6>  
				</div>
				  <div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/forks/') }}"><img src="{{ url('assets/images/icons/fork.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Forks</h6>  
				</div>
				  <div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/rims/') }}"><img src="{{ url('assets/images/icons/rims.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Rims</h6>  
				</div>
			</div>
		  </div>
		  <div class="container">
  			<div class="row">
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/grips/') }}"><img src="{{ url('assets/images/icons/grip.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Grips</h6>  
				</div>
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/headset/') }}"><img src="{{ url('assets/images/icons/headset-copy.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Headsets</h6>  
				</div>
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/hubs/') }}"><img src="{{ url('assets/images/icons/hubs.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Hubs</h6>  
				</div>
				  <div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/pedals/') }}"><img src="{{ url('assets/images/icons/pedal.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Pedals</h6>  
				</div>
				  <div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/pegs/') }}"><img src="{{ url('assets/images/icons/pegs-1.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Pegs</h6>  
				</div>
				  <div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/seat/') }}"><img src="{{ url('assets/images/icons/seat.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Seating</h6>  
				</div>
			</div>
		  </div>
		  <div class="container">
  			<div class="row">
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/smallparts/') }}"><img src="{{ url('assets/images/icons/small-parts-1.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Small Parts</h6>  
				</div>
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/spokes/') }}"><img src="{{ url('assets/images/icons/spokes.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">spokes</h6>  
				</div>
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/sprockets/') }}"><img src="{{ url('assets/images/icons/sprocket.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Sprockets</h6>  
				</div>
				  <div class="col-sm">
				  <div class="item"><a href="{{ url('/parts/stems/') }}"><img src="{{ url('assets/images/icons/stem.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Stems</h6>  
				</div>
				  <div class="col-sm">
				  <div class="item"><a href="{{ url('parts/tires/') }}"><img src="{{ url('assets/images/icons/tires.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Tires & Tubes</h6>  
				</div>
				  <div class="col-sm">
				  <div class="item"><a href="{{ url('parts/tools/') }}"><img src="{{ url('assets/images/icons/tools.jpg') }}" height="166" width="110" alt="images"></a> </div>
				  <h6 class="product-name">Tools</h6>  
				</div>
			</div>
  		</div>
  	</div>
  </section>
  <!-- Main Container End -->
  <br>
  <br>
  <br>
  @endsection