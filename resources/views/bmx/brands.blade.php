  @extends('layout/header')

  @section('title', 'Mic Ride - All brands sold here ')

  @section('container')
  <!-- Main Container -->
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <section class="main-container col1-layout bounceInUp animated">
  	<div class="main container">
  		<div class="container">
  			<div class="row">
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/anml-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">ANIMAL BIKES</h6>
  				</div>
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/bell-helmet-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">BELL HELMET</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/bsd-forever-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">BSD FOREVER</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/CINEMA-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">CINEMA</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/dia-compe-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">DIA COMPE</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/ECLAT-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">ECLAT</h6>
  				</div>
			</div>
          </div>
          <div class="container">
  			<div class="row">
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/federal-bikes-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">FEDERAL</h6>
  				</div>
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/gsportbw-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">G SPORT</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/hyper-bmx-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">HYPER</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/khe-bikes-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">KHE BIKES</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/merritt-bmx-150x150.png') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">MERRITT</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/MISSION-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">MISSION</h6>
  				</div>
			</div>
          </div>
          <div class="container">
  			<div class="row">
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/odyssey-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">ODYSSEY</h6>
  				</div>
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/PROFILE-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">PROFILE RACING</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/PROTEC-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">PROTEC</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/snm-bikes-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">S&M BIKES</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/SUBROSA-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">SUBROSA</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/sunday-bikes-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">SUNDAY</h6>
  				</div>
			</div>
          </div>
          <div class="container">
  			<div class="row">
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/T1-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">TERRIBLE ONE</h6>
  				</div>
  				<div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/TSC-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">THE SHADOW CONSPIRACY</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/WTP-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">WE THE PEOPLE</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/snm-bikes-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">S&M BIKES</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/ODI-150x150.jpg') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">ODI GRIPS</h6>
                  </div>
                  <div class="col-sm">
				  <div class="item"><a href="{{ url('') }}"><img src="{{ url('assets/images/logo/FIT-BIKE-CO.png') }}" height="150" width="150" alt="images"></a> </div>
				  <h6 class="product-name">FIT BIKE CO</h6>
  				</div>
			</div>
		  </div>
  	</div>
  </section>
  <!-- Main Container End -->
  <br>
  <br>
  <br>
  @endsection