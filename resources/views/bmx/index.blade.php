@extends('layout/header')

@section('title', 'Mic Ride - Welcome, Build your bike now !')

@section('container')
    <!-- Start Search Popup -->
		<div class="brown--color box-search-content search_active block-bg close__top">
			<form id="search_mini_form" class="minisearch" action="#">
				<div class="field__search">
					<input type="text" placeholder="Search entire store here...">
					<div class="action">
						<a href="#"><i class="zmdi zmdi-search"></i></a>
					</div>
				</div>
			</form>
			<div class="close__wrap">
				<span>close</span>
			</div>
		</div>
		<!-- End Search Popup -->
		<!-- Start Slider area -->
		<div class="slider-area brown__nav slider--15 slide__activation slide__arrow01 owl-carousel owl-theme">
			<!-- Start Single Slide -->
			<div class="slide animation__style10 bg-image--1 fullscreen align__center--left">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="slider__content">
								<div class="contentbox">
									<h2>Build <span>your </span></h2>
									<h2>bike <span>now , </span></h2>
									<h2><span>Here </span></h2>
									<a class="shopbtn" href="{{ url('/parts') }}">shop now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Single Slide -->
			<!-- Start Single Slide -->
			<div class="slide animation__style10 bg-image--7 fullscreen align__center--left">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="slider__content">
								<div class="contentbox">
									<h2>Buy <span>the </span></h2>
									<h2>parts <span>you </span></h2>
									<h2>need <span>Here </span></h2>
									<a class="shopbtn" href="{{ url('/parts') }}">shop now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Single Slide -->
		</div>
		<!-- End Slider area -->
		

        <!-- End Shop Page -->


		<!-- Start Recent Post Area -->
		<section class="wn__recent__post bg--gray ptb--80">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="section__title text-center">
							<h2 class="title__be--2">News</h2>
						</div>
					</div>
				</div>
				<div class="row mt--50">
					@foreach ($blog as $blog)
					<?php
					$isi = substr($blog->isi, 0, 300 );
					?>
					<div class="col-md-6 col-lg-4 col-sm-12">
						<div class="post__itam">
							<div class="content">
								<h3><a href="{{ url('blog/' . $blog->id) }}">{{ $blog->judul }}</a></h3>
								<p>{!!html_entity_decode($isi)!!}.......</p>
								<div class="post__time">
									<span class="day">{{ $blog->created_at }}</span>
									<div class="post-meta">
										<ul>
											<li><a href="#"><i class="bi bi-love"></i>72</a></li>
											<li><a href="#"><i class="bi bi-chat-bubble"></i>27</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</section>
		
		<!-- End Recent Post Area -->

	@endsection